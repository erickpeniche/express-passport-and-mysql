module.exports = function(db) {
	var passport = require('passport');
	var LocalStrategy = require('passport-local').Strategy;

	// =========================================================================
	// passport session setup ==================================================
	// =========================================================================
	// required for persistent login sessions
	// passport needs ability to serialize and unserialize users out of session

	// used to serialize the user for the session
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	// used to deserialize the user
	passport.deserializeUser(function(id, done) {
		//console.log("Id: " + id);
		db.query(
			"select * from users where id = " + id,
			function(err,rows){
				done(err, rows[0]);
			}
		);
	});

	passport.use(new LocalStrategy({
			usernameField: 'username',
			passwordField: 'password'
		},
		function(username, password, done) {
			//console.log("query: " + "select * from users where username = '" + username + "' and password = '" + password + "'");
			db.query(
				"select * from users where username = '" + username + "' and password = '" + password + "'",
				function(err, rows) {
					//console.log(rows);
					if (err) {
						console.error(err);
						return done(err);
					} else {
						return done(null, rows[0]);
					}
				}
			);
		}
	));

	return passport;
}