// Create schema query
connection.query(
    "CREATE SCHEMA `my_example` DEFAULT CHARACTER SET utf8",
    function(err) {
        if (err) {
            console.error("There was an error running your query");
            console.error("Error code: " + err.code);
        } else {
            console.log('The schema my_example was created successfully!');
        }
    }
);

// List all schemas
connection.query(
    "SHOW DATABASES",
    function(err, rows) {
        if (err) {
            console.error("There was an error running your query");
            console.error("Error code: " + err.code);
        } else {
            console.dir(rows);
        }
    }
);

// Create table query
connection.query(
	"CREATE TABLE IF NOT EXISTS `my_example`.`users` ("
	+ "`id` INT NOT NULL AUTO_INCREMENT, "
	+ "`name` VARCHAR(255) NULL, "
	+ "`age` INT NULL, "
	+ "PRIMARY KEY (`id`)) "
	+ "ENGINE = InnoDB "
	+ "DEFAULT CHARACTER SET = utf8",
	function(err) {
	    if (err) {
	        console.error("There was an error running your query");
	        console.error("Error code: " + err.code);
	    } else {
	        console.log('The table "users" was created successfully!');
	    }
	}
);

// List table from my_example
connection.query(
    "SHOW TABLES FROM my_example",
    function(err, rows) {
        if (err) {
            console.error("There was an error running your query");
            console.error("Error code: " + err.code);
        } else {
            console.dir(rows);
        }
    }
);

// Add entries to the users table
connection.query(
    "INSERT INTO users (name, age) "
    + "VALUES ('Erick', 23), ('John', 20), ('Kevin', 24)",
    function(err, result) {
        if (err) {
            console.error("There was an error running your query");
            console.error("Error code: " + err.code);
        } else {
            console.log(result.affectedRows + " rows were inserted successfully!");
        }
    }
);

// select all users in the table
connection.query(
    "SELECT * FROM users",
    function(err, result) {
        if (err) {
            console.error("There was an error running your query");
            console.error("Error code: " + err.code);
        } else {
            console.log("There are " + result.length + " user in the table");
            console.dir(result);
            for (index in result) {
                var user = result[index];
                console.log("Hello, my name is " + user.name + " and I am " + user.age + " years old.");
            }
        }
    }
);

/***********************************************************
 * Alter the table structure for authentication my_example *
 ***********************************************************/

// Alter table
connection.query(
    "ALTER TABLE users "
    + "ADD COLUMN `username` VARCHAR(255) NOT NULL, "
    + "ADD COLUMN `password` VARCHAR(45) NOT NULL",
    function(err) {
        if (err) {
            console.error("There was an error running your query");
            console.error("Error code: " + err.code);
        }
    }
);

// Updating users information
connection.query(
    "UPDATE users SET `username`='erick', `password`='test' WHERE `id`='1'"
    function(err) {
        if (err) {
            console.error("There was an error running your query");
            console.error("Error code: " + err.code);
        }
    }
);