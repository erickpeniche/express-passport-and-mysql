module.exports = function(passport, connection) {
	var express = require('express');
	var router = express.Router();

	/* GET Admin page. */
	router.get('/', function(req, res) {
		if (req.isAuthenticated()) {
			res.redirect('/admin/home');
		}
		res.render('admin', {
			title: 'Admin Access',
			message: req.flash('notAuthenticated'),
			flash: req.flash('error')
		});
	});

	router.post('/',
		passport.authenticate('local',
		{
			successRedirect: '/admin/home',
			failureRedirect: '/admin',
			failureFlash: 'Invalid username or password.',
			successFlash: 'You have successfully logged in!'
		})
	);

	router.get('/home', isLoggedIn, function(req, res) {
		res.render('admin_home', {
			title: 'Welcome!',
			flash: req.flash('success')
		});
	});

	return router;
}

// route middleware to make sure
function isLoggedIn(req, res, next) {
	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the login page
	req.flash('notAuthenticated', 'You need to login before entering the requested page!');
	res.redirect('/admin');
}